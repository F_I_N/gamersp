package com.example.gamersp

class Song(val title: String, val artist: String) {
    fun play() {
        println("Играет песня $title группы $artist")
    }

    fun stop() {
        println("Остановка проигрывания $title")
    }
}

fun main() {
    val songOne = Song("Королева Фейк", "Jane Air")
    val songTwo = Song("ЧБ дни", "Amatory")
    val songThree = Song("MMM", "Психея")
    songOne.play()
    songOne.stop()
    songTwo.play()
    songTwo.stop()
    songThree.play()
    songThree.stop()
}