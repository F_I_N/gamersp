package com.example.gamersp

fun main() {
    var myShopList = mutableListOf("Tea", "Сахар","Milk","Eggs")
    println("Оригинальный список покупок: $myShopList")

    val extraShoping = listOf("Печенья", "Сахар", "Яйца")
    myShopList.addAll((extraShoping))
    println("Обновленный список покупок: $myShopList")

    if (myShopList.contains("Tea")) {
        myShopList.set(myShopList.indexOf("Tea"), "Coffee")
    }
    println(myShopList)

    myShopList.sort()
    println("Сортированный список покупок: $myShopList")

    myShopList.reverse()
    println("Перевернутый список покупок: $myShopList")



    val myShopSet = myShopList.toMutableSet()
    println("Set списка покупок $myShopSet")

    val moreShopping = setOf("Chives","Spinach","Milk")
    myShopSet.addAll(moreShopping)
    println("Добавленые элементы в Set $myShopSet")

    myShopList = myShopSet.toMutableList()
    println("Новая версия Списка покупок $myShopList")
}