package com.example.gamersp

interface Roamable {
    fun roam()
}



abstract class AnimalAbs : Roamable {
    abstract val image: String
    abstract val food: String
    abstract val habitat: String
    var hunger = 10


    abstract fun makeNoise()

    abstract fun eat()

    override fun roam() {
        println("Животное странствует")
    }

    fun sleep() {
        println("Животное спит")
    }

}



class HippoAbs : AnimalAbs() {
    override val image = "hippo.jpg"
    override val food = "трава"
    override val habitat = "вода"

    override fun makeNoise() {
        println("Хрюк хрюк")
    }

    override fun eat() {
        println("Гиппопотам ест $food")
    }
}



abstract class CanianeAbs : AnimalAbs() {
    override fun roam() {
        println("Собачьи передвигаются")
    }
}



class WolfAbs : CanianeAbs() {
    override val image = "wolf.jpg"
    override val food = "мясо"
    override val habitat = "леса"

    override fun makeNoise() {
        println("Воооой!")
    }

    override fun eat() {
        println("Волк есть $food")
    }
}

class Vehicle : Roamable {
    override fun roam() {
        println("Транспортное средство передвигается")
    }
}



class VetAbs {
    fun giveShot(animal: AnimalAbs) {
        animal.makeNoise()
    }
}



fun main() {
    val animalsAbs = arrayOf(HippoAbs(), WolfAbs())
    for (item in animalsAbs) {
        item.roam()
        item.eat()
        item.sleep()
    }

    println("---------------------")
    val vet = VetAbs()
    val wolf = WolfAbs()
    val hippo = HippoAbs()
    vet.giveShot(wolf)
    vet.giveShot(hippo)

    println("-------------------")

    val roamables = arrayOf(HippoAbs(), WolfAbs(), Vehicle())
    for (item in roamables) {
        item.roam()
        if (item is AnimalAbs) {
            item.eat()
        }
    }


}