package com.example.gamersp

open class Animal() {
    open val image = ""
    open val food = ""
    open val habitat = ""
    var hunger = 10


    open fun makeNoise() {
        println("Животное издает звук")
    }

    open fun eat() {
        println("Животное ест")
    }

    open fun roam() {
        println("Животное странствует")
    }

    fun sleep() {
        println("Животное спит")
    }

}

class Hippo : Animal() {
    override val image = "hippo.jpg"
    override val food = "трава"
    override val habitat = "вода"

    override fun makeNoise() {
        println("Хрюк хрюк")
    }

    override fun eat() {
        println("Гиппопотам ест $food")
    }
}

open class Caniane : Animal() {
    override fun roam() {
        println("Собачьи странствуют")
    }
}

class Wolf : Caniane() {
    override val image = "wolf.jpg"
    override val food = "мясо"
    override val habitat = "леса"

    override fun makeNoise() {
        println("Воооой!")
    }

    override fun eat() {
        println("Волк есть $food")
    }
}

class Vet {
    fun giveShot(animal: Animal) {
        animal.makeNoise()
    }
}

fun main() {
    val animals = arrayOf(Hippo(), Wolf())
    for (item in animals) {
        item.roam()
        item.eat()
        item.sleep()
    }

    println("---------------------")
    val vet = Vet()
    val wolf = Wolf()
    val hippo = Hippo()
    vet.giveShot(wolf)
    vet.giveShot(hippo)

}