package com.example.gamersp

class WolfNull {
    var hunger = 10
    var food = "мясо"

    fun eat() {
        println("Волк есть $food")
    }
}

class MyWolf {
    var wolf: WolfNull = WolfNull()


    fun myFunction() {
        wolf?.eat()
    }
}

fun getAlphaWolf() : WolfNull? {
    return WolfNull()
}

fun main() {
    var w: WolfNull? = WolfNull()

    if (w != null) {
        w.eat()
    }



    var x = w?.hunger
    println("The value of x is $x")


    var y = w?.hunger ?: -1
    println("The value of y is $y")


    var myWolf = MyWolf()
    println("Значение переменной myWolf?.wolf?.hunger ДО изменения равно ${myWolf?.wolf?.hunger}")
    myWolf?.wolf?.hunger = 8   // доступ
    println("Значение переменной myWolf?.wolf?.hunger после изменения равно ${myWolf?.wolf?.hunger}")


    var myArray = arrayOf("Hi", "Bye", null)
    for (item in myArray) item?.let { println(it) }  //вывод только не-null значений


    getAlphaWolf().let { it?.eat() }


    w = null
    var z = w!!.hunger  //Выдает ошибку так как значение равно null

}