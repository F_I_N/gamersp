package com.example.gamersp

class Dog(val name: String,
              weight_param: Int,
              breed_param: String) {

    init {
        print("Собака $name создана. ")
    }

    var activities = arrayOf("Walks")
    val breed = breed_param.toUpperCase()

    init {
        println("Порода - $breed")
    }

    var weight = weight_param
    set(value) {
        if (value > 0) field = value
    }

    val weightInKgs: Double
    get() = weight / 2.2

    fun bark() {
        println(if (weight <20) "Тяв" else "Гав")
    }

}

fun main() {
    val myDog = Dog("Чаппи", 20, "Дворняга")
    myDog.bark()    //вызов "голоса" собаки

    myDog.weight = 74
    println("Вес в килограммах равен ${myDog.weightInKgs}")

    myDog.weight = -2
    println("Вес равен ${myDog.weight}")

    myDog.activities = arrayOf("Проулки", "Ловить мячики","Фрисби")
    for (item in myDog.activities) {
        println("Моя собака любит $item")
    }

    val dogs = arrayOf(Dog("Белка", 40, "Охотничья"), Dog("Тузик", 35, "Овчарка"))

    dogs[1].bark()

    dogs[1].weight = 15
    dogs[1].bark()

    println("Вес собаки ${dogs[1].name} равен ${dogs[1].weight}")


}