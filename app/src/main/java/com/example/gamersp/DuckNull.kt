package com.example.gamersp

class Duck (val height: Int? = null) {
    fun quack() {
        println ("Ква! Ква! Ква!")
    }
}


class MyDucks (var myDucks: Array<Duck?>) {

    fun quack() {
        for (duck in myDucks) {
            duck?.let { it.quack() }
        }
    }
}


